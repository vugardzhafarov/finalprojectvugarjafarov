package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;

public class DatePickerPage extends BasePage {

    @FindBy(xpath = "//*[@id=\"post-2661\"]/div[2]/div/div/div[1]/p/iframe")
    public WebElement iFrameButton;
    @FindBy(id = "datepicker")
    public WebElement dataPicker;
    @FindBy(xpath = "//*[@id=\"ui-datepicker-div\"]/div/a[2]")
    public WebElement nextMonthButtonOnCalendar;
    @FindBy(xpath = "//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[5]/td[5]/a")
    public WebElement chooseDate;

    public DatePickerPage(WebDriver driver) {
        super(driver);
    }

    public void datepick() {
        driver.switchTo().frame(iFrameButton);
        dataPicker.click();
        nextMonthButtonOnCalendar.click();
        chooseDate.click();
    }

    public boolean isValidFormat(String format, String value, Locale locale) {
        LocalDateTime ldt = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format, locale);

        try {
            ldt = LocalDateTime.parse(value, formatter);
            String result = ldt.format(formatter);
            return result.equals(value);
        } catch (DateTimeParseException e) {
            try {
                LocalDate ld = LocalDate.parse(value, formatter);
                String result = ld.format(formatter);
                return result.equals(value);
            } catch (DateTimeParseException exp) {
                exp.printStackTrace();
            }
        }

        return false;
    }

}
