package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class DemoSitePage extends BasePage {
    @FindBy (xpath = "//li[text()=\"First Step\"]/following-sibling::li")
    public List<WebElement> firstStep;
    @FindBy (xpath = "//li[text()=\"Second Step\"]/following-sibling::li" )
    public  List<WebElement> secondStep;
    @FindBy (xpath = "//li[text()=\"Third Step\"]/following-sibling::li" )
    public List<WebElement> thirdStep;


    public DemoSitePage(WebDriver driver){
        super(driver);
    }

}
