package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProgressBarPage extends BasePage{

    @FindBy(xpath = "//*[@id=\"post-2671\"]/div[2]/div/div/div[1]/p/iframe")
    public WebElement iFrameButton;
    @FindBy(id = "downloadButton")
    public WebElement downloadButton;
    @FindBy(xpath = "//div[@class='progress-label']")
    public WebElement completeWord;

    public ProgressBarPage(WebDriver driver){
        super(driver);
    }

    public void clickToDownloadButton(){
        driver.switchTo().frame(iFrameButton);
        downloadButton.click();
    }

    public void waitDownload(){
        wait.until(ExpectedConditions.textToBePresentInElement(completeWord, "Complete!"));
    }

}
