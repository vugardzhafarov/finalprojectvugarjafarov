package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BasePage {
    WebDriver driver;
    WebDriverWait wait;
    @FindBy(xpath = "//*[@id=\"menu-item-2822\"]/a")
    public WebElement testersHub;
    @FindBy(xpath = "//*[@id=\"menu-item-2823\"]/a/span")
    public WebElement demoTesting;
    @FindBy(id = "menu-item-2827")
    public WebElement dataPickerMenu;
    @FindBy(xpath = "//*[@id=\"menu-item-2832\"]/a")
    public WebElement progressBarMenu;

    public BasePage(WebDriver driver) {
        wait = new WebDriverWait(driver, 30);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openDemoTestingPage() {
        Actions actions = new Actions(driver);
        actions.moveToElement(testersHub).build().perform();
        demoTesting.click();
    }

    public void openDatePickerPage() {
        Actions actions = new Actions(driver);
        actions.moveToElement(testersHub).moveToElement(demoTesting).perform();
        dataPickerMenu.click();
    }

    public void openProgressBarPage() {
        Actions actions = new Actions(driver);
        actions.moveToElement(testersHub).moveToElement(demoTesting).perform();
        progressBarMenu.click();
    }

}
