package Test;

import Pages.BasePage;
import Pages.DatePickerPage;
import Pages.DemoSitePage;
import Pages.ProgressBarPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Test_cases {

    WebDriver webDriver;
    WebDriverWait webDriverWait;
    BasePage basePage;
    DemoSitePage demoSitePage;
    DatePickerPage datePickerPage;
    ProgressBarPage progressBarPage;

    @BeforeMethod
    void beforeAllTests() {
        webDriver = new ChromeDriver();
        webDriverWait = new WebDriverWait(webDriver, 10);
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        webDriver.get("https://www.globalsqa.com/");
    }

    @AfterMethod
    void afterAllTests() {
        webDriver.quit();
    }

    @Test
    void demoTestingSiteHasSixItemsInEachColumn() {
        basePage = new BasePage(webDriver);
        demoSitePage = new DemoSitePage(webDriver);
        basePage.openDemoTestingPage();
        Assert.assertEquals(6, demoSitePage.firstStep.size());
        Assert.assertEquals(6, demoSitePage.secondStep.size());
        Assert.assertEquals(6, demoSitePage.thirdStep.size());

    }

    @Test
    void validDateFormatInDatePickerPage() {
        basePage = new BasePage(webDriver);
        datePickerPage = new DatePickerPage(webDriver);
        basePage.openDatePickerPage();
        datePickerPage.datepick();
        Assert.assertTrue(datePickerPage.isValidFormat("MM/dd/yyyy", datePickerPage.dataPicker.getAttribute("value"), Locale.ENGLISH));
    }

    @Test
    void downloadInProgressBarPage() {
        basePage = new BasePage(webDriver);
        progressBarPage = new ProgressBarPage(webDriver);
        basePage.openProgressBarPage();
        progressBarPage.clickToDownloadButton();
        progressBarPage.waitDownload();
        Assert.assertTrue(progressBarPage.completeWord.isDisplayed());
    }
}
